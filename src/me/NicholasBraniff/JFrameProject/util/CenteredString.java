package me.NicholasBraniff.JFrameProject.util;

import java.awt.Graphics2D;
import java.awt.font.FontRenderContext;
import java.awt.font.GlyphVector;
import java.awt.geom.Rectangle2D;

public class CenteredString {

	private int x;
	private int y;
	private String s;
	
	public CenteredString(String s, Graphics2D g2d, int width, int height){
		this.s = s;
		centerString(g2d, height, width);
	}
	
	public CenteredString(String s, Graphics2D g2d, int width, int height, int widthOffSet, int heightOffSet){
		this.s = s;
		offSetCenterString(g2d, height, width, widthOffSet, heightOffSet);
	}
	
	private void centerString(Graphics2D g2d, int height, int width){
		FontRenderContext frc = g2d.getFontRenderContext();
        GlyphVector gv = g2d.getFont().createGlyphVector(frc, s);
        Rectangle2D box = gv.getVisualBounds();

        x = (int) ((width - (int) box.getWidth()) / 2);
        y = (int)(((height - box.getHeight()) / 2d) + (-box.getY()));
	}
	
	private void offSetCenterString(Graphics2D g2d, int height, int width, int widthOffSet, int heightOffSet){
		FontRenderContext frc = g2d.getFontRenderContext();
        GlyphVector gv = g2d.getFont().createGlyphVector(frc, s);
        Rectangle2D box = gv.getVisualBounds();

        x = (int) ((width - (int) box.getWidth()) / 2);
        y = (int)(((height - box.getHeight()) / 2d) + (-box.getY()));
        x += widthOffSet;
        y += heightOffSet;
	}

	public int getWidth() {
		return x;
	}

	public int getHeight() {
		return y;
	}

	public String getString() {
		return s;
	}
	
	
}
