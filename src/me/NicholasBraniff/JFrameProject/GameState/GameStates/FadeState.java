package me.NicholasBraniff.JFrameProject.GameState.GameStates;

import java.awt.Graphics2D;

import me.NicholasBraniff.JFrameProject.GameState.GameState;
import me.NicholasBraniff.JFrameProject.GameState.GameStateManager;
import me.NicholasBraniff.JFrameProject.TileMap.Fade;

public class FadeState extends GameState{

	private Fade fd;
	private int nextState;
	
	public FadeState(int nextState, GameStateManager gsm){
		try{
			fd = new Fade("/Backgrounds/fade.gif", nextState, gsm);
			this.nextState = nextState;
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	@Override
	public void init() {
	}

	@Override
	public void update() {
	}

	@Override
	public void draw(Graphics2D g) {
		fd.draw(g);
	}

	@Override
	public void keyPressed(int k) {
	}

	@Override
	public void keyReleased(int k) {
	}

	public int getNextState() {
		return nextState;
	}

	public void setNextState(int nextState) {
		this.nextState = nextState;
		fd.setNextState(this.nextState);
	}
	
	public void setImage(String image){
		fd.setImage(image);
	}
	
}
