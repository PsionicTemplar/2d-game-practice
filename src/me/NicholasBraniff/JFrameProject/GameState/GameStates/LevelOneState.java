package me.NicholasBraniff.JFrameProject.GameState.GameStates;

import java.awt.Graphics2D;

import me.NicholasBraniff.JFrameProject.GameState.GameState;
import me.NicholasBraniff.JFrameProject.GameState.GameStateManager;
import me.NicholasBraniff.JFrameProject.TileMap.Background;
import me.NicholasBraniff.JFrameProject.TileMap.TileMap;

public class LevelOneState extends GameState{
	
	private TileMap tilemap;
	private Background bg;

	public LevelOneState(GameStateManager gsm){
		this.gsm = gsm;
		init();
	}

	@Override
	public void init() {
		
		tilemap = new TileMap(32);
		tilemap.loadTiles("/Tilesets/house.gif");
		tilemap.loadMap("/Maps/testmap2.map");
//		tilemap = new TileMap(30);
//		tilemap.loadTiles("/Tilesets/grasstileset.gif");
//		tilemap.loadMap("/Maps/level1-1.map");
		tilemap.setPosition(0, 0);
		
		bg = new Background("/Backgrounds/grassbg1.gif", 0);
		
	}

	@Override
	public void update() {
	}

	@Override
	public void draw(Graphics2D g) {
		bg.draw(g);
		
		tilemap.draw(g);
	}

	@Override
	public void keyPressed(int k) {
	}

	@Override
	public void keyReleased(int k) {
	}
	
}
