package me.NicholasBraniff.JFrameProject.GameState.GameStates;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;

import me.NicholasBraniff.JFrameProject.GamePanel;
import me.NicholasBraniff.JFrameProject.GameState.GameState;
import me.NicholasBraniff.JFrameProject.GameState.GameStateManager;
import me.NicholasBraniff.JFrameProject.TileMap.Background;
import me.NicholasBraniff.JFrameProject.util.CenteredString;

public class MenuState extends GameState {

	private Background bg;

	private int currentChoice = 0;
	private String[] options = { "New Game", "Continue", "Quit" };
	
	private Color titleColor;
	private Font titleFont;
	
	private Font font;

	public MenuState(GameStateManager gsm) {
		this.gsm = gsm;
		
		try{
			bg = new Background("/Backgrounds/menubg.gif", 1);
			bg.setVector(-0.1, 0);
			
			titleColor = new Color(226, 124,72);
			titleFont = new Font("Freestyle Script", Font.PLAIN, 48);
			
			font  = new Font("Arial", Font.PLAIN, 12);
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}

	@Override
	public void init() {
	}

	@Override
	public void update() {
		bg.update();

	}

	@Override
	public void draw(Graphics2D g) {
		bg.draw(g);
		
		g.setColor(titleColor);
		g.setFont(titleFont);
		CenteredString title = new CenteredString("Fall Hollow", g, GamePanel.WIDTH, GamePanel.HEIGHT, 0, -50);
		g.drawString(title.getString(), title.getWidth(), title.getHeight());

		g.setFont(font);
		title = new CenteredString(options[0], g, GamePanel.WIDTH, GamePanel.HEIGHT, 0, 30);
		for(int i = 0; i < options.length; i++){
			CenteredString s = new CenteredString(options[i], g, GamePanel.WIDTH, GamePanel.HEIGHT, 0, 30);
			if(i == currentChoice){
				g.setColor(Color.BLACK);
			}else{
				g.setColor(new Color(0, 82, 93));
			}
			g.drawString(options[i], s.getWidth(), title.getHeight() + i * 15);
		}
	}
	
	private void select(){
		if(currentChoice == 0){
			gsm.setFadeState(2, true);
		}
		if(currentChoice == 2){
			System.exit(0);
		}
	}

	@Override
	public void keyPressed(int k) {
		if(k == KeyEvent.VK_ENTER){
			select();
		}
		if(k == KeyEvent.VK_UP){
			currentChoice--;
			if(currentChoice == -1){
				currentChoice = options.length - 1;
			}
		}
		
		if(k == KeyEvent.VK_DOWN){
			currentChoice++;
			if(currentChoice == options.length){
				currentChoice = 0;
			}
		}

	}

	@Override
	public void keyReleased(int k) {
		// TODO Auto-generated method stub

	}

}