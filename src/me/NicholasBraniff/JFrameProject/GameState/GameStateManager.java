package me.NicholasBraniff.JFrameProject.GameState;

import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.List;

import me.NicholasBraniff.JFrameProject.GameState.GameStates.FadeState;
import me.NicholasBraniff.JFrameProject.GameState.GameStates.LevelOneState;
import me.NicholasBraniff.JFrameProject.GameState.GameStates.MenuState;

public class GameStateManager {

	private List<GameState> gameStates;
	private int currentState;

	private static final int MENUSTATE = 0;
	private static final int FADESTATE = 1;
	private static final int LEVELONESTATE = 2;
	
	private boolean isFadeState = false;

	public GameStateManager() {
		gameStates = new ArrayList<GameState>();

		currentState = getMenuState();
		gameStates.add(new MenuState(this));
		gameStates.add(new FadeState(0, this));
		gameStates.add(new LevelOneState(this));
	}

	public void setCurrentState(int state) {
		currentState = state;
		gameStates.get(currentState).init();
	}

	public void setFadeState(int nextState, boolean isFadeState) {
		this.isFadeState = isFadeState;
		FadeState f = (FadeState) gameStates.get(1);
		f.setNextState(nextState);
		currentState = FADESTATE;
		gameStates.get(currentState).init();
	}

	public void update() {
		gameStates.get(currentState).update();
	}

	public void draw(Graphics2D g) {
		if (isFadeState()) {
			for (int i = 0; i < 5; i++) {
				gameStates.get(0).update();
				gameStates.get(0).draw(g);
				try {
					Thread.sleep(2);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			gameStates.get(currentState).draw(g);

		} else {
			gameStates.get(currentState).draw(g);
		}
	}

	public void keyPressed(int k) {
		gameStates.get(currentState).keyPressed(k);
	}

	public void keyReleased(int k) {
		gameStates.get(currentState).keyReleased(k);
	}

	public int getMenuState() {
		return GameStateManager.MENUSTATE;
	}

	public int getLevelOneState() {
		return GameStateManager.LEVELONESTATE;
	}

	public boolean isFadeState() {
		return isFadeState;
	}
	
	public GameState getGameState(int state){
		return gameStates.get(state);
	}
}