package me.NicholasBraniff.JFrameProject.Entity;

import java.awt.image.BufferedImage;
import java.util.ArrayList;

import javax.imageio.ImageIO;

import me.NicholasBraniff.JFrameProject.TileMap.TileMap;

public class Player extends Entity{
	
	private int health;
	private int maxHealth;
	
	private boolean dead;
	private boolean flinch;
	private long flinchTimer;
	
	private ArrayList<BufferedImage[]> sprites;
	private final int[]	 numFrames = {0,0,0,0,0,0,0,0};
	
	public static final int IDLEUP = 0;
	public static final int IDLEDOWN = 1;
	public static final int IDLELEFT = 2;
	public static final int IDLERIGHT = 3;
	public static final int MOVEUP = 4;
	public static final int MOVEDOWN = 5;
	public static final int MOVELEFT = 6;
	public static final int MOVERIGHT = 7;
	
	private int currentAction;
	
	
	public Player(TileMap tm){
		super(tm);
		
		width = 32;
		height = 32;
		cwidth = 20;
		cheight = 20;
		moveSpeed = 0.3;
		maxSpeed = 1.6;
		stopSpeed = 0.4;
		
		this.facingDown = true;
		this.facingRight = false;
		
		health = maxHealth = 5;
		
		try{
			
			BufferedImage spriteSheet = ImageIO.read(getClass().getResourceAsStream("/Sprites/playersprites.gif"));
			
			for(int i = 0; i < 8; i++){
				BufferedImage[] bi = new BufferedImage[numFrames[i]];
				for(int j = 0; j < numFrames[i]; j++){
					bi[j] = spriteSheet.getSubimage(j * width, i * height, width, height);
				}
				sprites.add(bi);
			}
			
		} catch(Exception ex){
			ex.printStackTrace();
		}
		
		currentAction = IDLEDOWN;
	}
	
	public void update(){
		getNextPosition();
		checkTileMapColision();
		setPosition(xTemp, yTemp);
		
		if(dy > 0){
			if(!this.facingDown){
				this.facingDown = true;
			}
			currentAction = MOVEUP;
			animation.setFrames(sprites.get(MOVEUP));
			animation.setDelay(100);
			width = 32;
		}
		
	}


	public int getHealth() {
		return health;
	}


	public void setHealth(int health) {
		this.health = health;
	}


	public int getMaxHealth() {
		return maxHealth;
	}


	public void setMaxHealth(int maxHealth) {
		this.maxHealth = maxHealth;
	}


	public boolean isDead() {
		return dead;
	}


	public void setDead(boolean dead) {
		this.dead = dead;
	}


	public boolean isFlinch() {
		return flinch;
	}


	public void setFlinch(boolean flinch) {
		this.flinch = flinch;
	}


	public long getFlinchTimer() {
		return flinchTimer;
	}


	public void setFlinchTimer(long flinchTimer) {
		this.flinchTimer = flinchTimer;
	}
	
	
	
	

}
