package me.NicholasBraniff.JFrameProject.Entity;

import java.awt.Rectangle;

import me.NicholasBraniff.JFrameProject.GamePanel;
import me.NicholasBraniff.JFrameProject.TileMap.Tile;
import me.NicholasBraniff.JFrameProject.TileMap.TileMap;

public abstract class Entity {
	protected TileMap tileMap;
	protected int tileSize;
	protected double xmap;
	protected double ymap;

	protected double x;
	protected double y;
	protected double dx;
	protected double dy;

	protected int width;
	protected int height;

	protected int cwidth;
	protected int cheight;

	protected int currRow;
	protected int currCol;
	protected double xDest;
	protected double yDest;
	protected double xTemp;
	protected double yTemp;
	protected boolean topLeft;
	protected boolean topRight;
	protected boolean bottomLeft;
	protected boolean bottomRight;

	protected Animation animation;
	protected int currentAction;
	protected int prevAction;
	protected boolean facingRight;
	protected boolean facingDown;

	protected boolean left;
	protected boolean right;
	protected boolean up;
	protected boolean down;

	protected double moveSpeed;
	protected double maxSpeed;
	protected double stopSpeed;

	public Entity(TileMap tm) {
		tileMap = tm;
		tileSize = tm.getTileSize();
	}

	public boolean intersects(Entity e) {
		Rectangle r1 = getRectangle();
		Rectangle r2 = e.getRectangle();
		return r1.intersects(r2);
	}

	public Rectangle getRectangle() {
		return new Rectangle((int) x - cwidth, (int) y - cheight, cwidth, cheight);
	}

	public void calculateCorners(double x, double y) {

		int leftTile = (int) (x - cwidth / 2 + 1) / tileSize;
		int rightTile = (int) (x + cwidth / 2 - 1) / tileSize;
		int topTile = (int) (y - cheight / 2 + 1) / tileSize;
		int bottomTile = (int) (y + cheight / 2 - 1) / tileSize;

		int topLeft = tileMap.getType(topTile, leftTile);
		int topRight = tileMap.getType(topTile, rightTile);
		int bottomLeft = tileMap.getType(bottomTile, leftTile);
		int bottomRight = tileMap.getType(bottomTile, rightTile);

		this.topLeft = topLeft == Tile.BLOCKED;
		this.topRight = topRight == Tile.BLOCKED;
		this.bottomLeft = bottomLeft == Tile.BLOCKED;
		this.bottomRight = bottomRight == Tile.BLOCKED;
	}

	public void checkTileMapColision() {

		currCol = (int) x / tileSize;
		currRow = (int) y / tileSize;

		xDest = x + dx;
		yDest = y + dy;

		xTemp = x;
		yTemp = y;

		calculateCorners(x, yDest);
		if (dy < 0) {
			if (topLeft || topRight) {
				dy = 0;
				yTemp = currRow * tileSize + cheight / 2;
			} else {
				yTemp += dy;
			}
		}

		if (dy > 0) {
			if (bottomLeft || bottomRight) {
				dy = 0;
				yTemp = (currRow + 1) * tileSize + cheight / 2;
			} else {
				yTemp += dy;
			}
		}

		calculateCorners(xDest, y);
		if (dx < 0) {
			if (topLeft || bottomLeft) {
				dx = 0;
				xTemp = currCol * tileSize + cwidth / 2;
			} else {
				xTemp += dx;
			}
		}

		if (dx > 0) {
			if (topRight || bottomRight) {
				dx = 0;
				xTemp = (currCol + 1) * tileSize + cwidth / 2;
			} else {
				xTemp += dx;
			}
		}

	}

	public void setPosition(double x, double y) {
		this.x = x;
		this.y = y;
	}

	public void setVector(double dx, double dy) {
		this.dx = dx;
		this.dy = dy;
	}

	public void setLocalPosition() {
		xmap = tileMap.getX();
		ymap = tileMap.getY();
	}

	public void setLeft(Boolean b) {
		this.left = b;
	}

	public void setRight(Boolean b) {
		this.right = b;
	}

	public void setUp(Boolean b) {
		this.up = b;
	}

	public void setDown(Boolean b) {
		this.down = b;
	}

	public boolean notOnScreen() {
		return x + xmap + width < 0 || x + xmap - width > GamePanel.WIDTH || y + ymap + height < 0
				|| y + ymap - height > GamePanel.HEIGHT;
	}

	public int getX() {
		return (int) x;
	}

	public int getY() {
		return (int) y;
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public int getCwidth() {
		return cwidth;
	}

	public int getCheight() {
		return cheight;
	}

}
