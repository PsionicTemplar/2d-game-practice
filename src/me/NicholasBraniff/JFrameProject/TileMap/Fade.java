package me.NicholasBraniff.JFrameProject.TileMap;

import java.awt.AlphaComposite;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;

import javax.imageio.ImageIO;

import me.NicholasBraniff.JFrameProject.GameState.GameStateManager;

public class Fade {

	private BufferedImage image;

	public static String FADEIMAGE = "/Backgrounds/fade.gif";

	private double x;
	private double y;
	private GameStateManager gsm;

	private int nextState;

	private float alpha = 0.0f;

	public Fade(String s, int nextState, GameStateManager gsm) {
		try {
			image = ImageIO.read(getClass().getResourceAsStream(s));
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		this.gsm = gsm;
		this.nextState = nextState;
	}

	public void setPosition(double x, double y) {
		this.x = x;
		this.y = y;
	}

	public void draw(Graphics2D g) {

		Graphics2D g2d = (Graphics2D) g;

		// set the opacity
		g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, alpha));
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

		// do the drawing here
		if (gsm.isFadeState()) {
			g2d.drawImage(image, (int) x, (int) y, null);
		} else {
			gsm.getGameState(nextState).draw(g2d);
		}

		// increase the opacity and repaint
		alpha += 0.05f;
		if (alpha >= 1.0f) {
			alpha = 0.0f;
			if (gsm.isFadeState()) {
				gsm.setFadeState(nextState, false);
			} else {
				gsm.setCurrentState(nextState);
			}
		}

		// sleep for a bit
		try {
			Thread.sleep(100);
		} catch (Exception e) {

			e.printStackTrace();
		}
	}

	public void setImage(String s) {
		try {
			image = ImageIO.read(getClass().getResourceAsStream(s));
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		alpha = 0.00f;
	}
	
	public void setNextState(int i){
		this.nextState = i;
	}
}
