package me.NicholasBraniff.JFrameProject.TileMap;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

import javax.imageio.ImageIO;

import me.NicholasBraniff.JFrameProject.GamePanel;

public class TileMap {

	private double x;
	private double y;
	
	private int xmin;
	private int ymin;
	private int xmax;
	private int ymax;
	
	private double tween;
	
	private int[][] map;
	private int tileSize;
	private int numRows;
	private int numCols;
	private int width;
	private int height;
	
	private BufferedImage tileset;
	private int numTilesAcross;
	private Tile[][] tiles;
	
	private int rowOffset;
	private int colOffset;
	private int numRowsToDraw;
	private int numColsToDraw;
	
	public TileMap(int tileSize){
		this.tileSize = tileSize;
		this.numRowsToDraw = GamePanel.HEIGHT / tileSize + 2;
		this.numColsToDraw = GamePanel.WIDTH / tileSize + 2;
		tween = 0.07;
	}
	
	public void loadTiles(String s){
		try{
			
			tileset = ImageIO.read(getClass().getResourceAsStream(s));
			this.numTilesAcross = this.tileset.getWidth() / this.tileSize;
			tiles = new Tile[2][numTilesAcross];
			
			BufferedImage subimage;
			for(int col = 0; col < numTilesAcross; col++){
				subimage = tileset.getSubimage(col * tileSize, 0, tileSize, tileSize);
				tiles[0][col] = new Tile(subimage, Tile.NORMAL);
				subimage = tileset.getSubimage(col * tileSize, tileSize, tileSize, tileSize);
				tiles[1][col] = new Tile(subimage, Tile.BLOCKED);
			}
			
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
	
	public void loadMap(String s){
		try{
			
			InputStream in = getClass().getResourceAsStream(s);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			
			numCols = Integer.parseInt(br.readLine());
			numRows = Integer.parseInt(br.readLine());
			map = new int[numRows][numCols];
			width = numCols * tileSize;
			height = numRows * tileSize;
			
			String delims = "\\s+";
			String line = br.readLine();
			String[] tokens = line.split(delims);
			int counter = 0;
			for(int row = 0; row < numRows; row++){
				for(int col = 0; col < numCols; col++){
					map[row][col] = Integer.parseInt(tokens[counter]) - 1;
					if(map[row][col] < 0){
						map[row][col] = 0;
					}
					counter++;
				}
			}
			
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	public int getX() {
		return (int) x;
	}

	public int getY() {
		return (int) y;
	}

	public int getTileSize() {
		return tileSize;
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}
	
	public int getType(int row, int col){
		int rc = map[row][col];
		int r = rc / numTilesAcross;
		int c = rc % numTilesAcross;
		return tiles[r][c].getType();
	}
	
	public void setPosition(double x, double y){
		this.x += (x - this.x) * tween;
		this.y += (y - this.y) * tween;
		
		fixBounds();
		
		colOffset = (int) -this.x / tileSize;
		rowOffset = (int) -this.y / tileSize;
	}
	
	private void fixBounds(){
		if(x < xmin) x = xmin;
		if(y < ymin) y = ymin;
		if(x > xmax) x = xmax;
		if(y > ymax) y = ymax;
	}
	
	public void draw(Graphics2D g){
		
		for(int row = rowOffset;  row < rowOffset + this.numRowsToDraw; row++){
			
			if(row >= numRows){
				break;
			}
			
			for(int col = colOffset; col < colOffset + this.numColsToDraw; col++){
				if(col >= numCols){
					break;
				}
				
				if(map[row][col] == 0){
					continue;
				}
				
				int rc = map[row][col];
				int r = rc / this.numTilesAcross;
				int c = rc % this.numTilesAcross;
				
				g.drawImage(tiles[r][c].getImage(), (int) x + col * tileSize, (int) y + row * tileSize, null);
				
			}
		}
		
	}
	
}
